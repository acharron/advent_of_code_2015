import java.io.File

fun main(args: Array<String>) {
    val input = File("src/input_03.txt").readLines().first()

    var x = 0
    var y = 0

    var x2 = 0
    var y2 = 0

    val part2 = true
    var count = 0

    val visitedHouses = mutableListOf("$x-$y")

    for (char in input) {
        if (part2 && count % 2 == 0) {
            when (char) {
                '^' -> y2++
                '>' -> x2++
                'v' -> y2--
                '<' -> x2--
            }
            visitedHouses.add("$x2-$y2")
        } else {
            when (char) {
                '^' -> y++
                '>' -> x++
                'v' -> y--
                '<' -> x--
            }
            visitedHouses.add("$x-$y")
        }

        count++
    }

    println("Part ${if (part2) "2" else "1"} : ${visitedHouses.distinct().size}")
}