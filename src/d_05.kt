import java.io.File

fun main(args: Array<String>) {
    // Part 1
    val reg3vowels = Regex("""([a-zA-Z]*[aeiou][a-zA-Z]*){3,}""")
    val regLetterTwice = Regex("""(\w)\1""")
    val regForbidden = Regex("""(ab)|(cd)|(pq)|(xy)""")

    // Part 2
    val reg2Pairs = Regex("""(\w{2})\w*\1""")
    val regRepeatWithInbetween = Regex("""(\w)\w\1""")

    val input = File("src/input_05.txt").readLines()

    var niceStrings1 = 0
    var niceStrings2 = 0

    for (row in input) {
        if (!row.contains(regForbidden)) {
            if (row.contains(reg3vowels) && row.contains(regLetterTwice)) niceStrings1++
        }

        if (row.contains(reg2Pairs) && row.contains(regRepeatWithInbetween)) niceStrings2++
    }

    println("Part 1 : $niceStrings1")
    println("Part 2 : $niceStrings2")
}