import java.io.File

fun main(args: Array<String>) {
    val input = File("src/input_01.txt").readLines().first()

    var floor = 0
    var count = 1
    var resPart2 = -1

    for (char in input) {
        if (char == '(')  floor++
        if (char == ')') floor--

        if (floor == -1 && resPart2 == -1) resPart2 = count

        count++
    }

    println(floor)
    println(resPart2)
}